import sys
import os

def CreateIfNotExists(file: str):
    os.makedirs(os.path.dirname(file), exist_ok=True)
    if not os.path.exists(file):
        with open(file, 'x') as fp:
            pass


if __name__ == "__main__":
    day = int(sys.argv[1])

    test_file = f"tests/Day{day:02d}Tests.cpp"

    if not os.path.exists(test_file):
        with open(f"tests/Day{day:02d}Tests.cpp", "w") as file:
            file.write("#include <catch2/catch_test_macros.hpp>\n")
            file.write("#include <catch2/generators/catch_generators.hpp>\n")
            file.write("\n")
            file.write("#include <sstream>\n")
            file.write("\n")
            file.write(f"#include \"Day{day:02d}.hpp\"\n")
            file.write("\n")
            file.write(f"TEST_CASE(\"TODO Day{day:02d}\", \"[Day{day:02d}]\")\n")
            file.write("{\n")
            file.write("    CHECK(false);\n")
            file.write("}\n")

    CreateIfNotExists(f"data/day{day:02d}/test.txt")
    CreateIfNotExists(f"data/day{day:02d}/input.txt")
