def generate():
    
    include_dir = "src"
    source_dir = "src"

    for day in range(1,26):
        with open(f"{include_dir}/Day{day:02d}.hpp", "w") as file:
            file.write("#pragma once\n")
            file.write("\n")
            file.write("#include \"advent.hpp\"\n")
            file.write("\n")
            file.write("namespace advent {\n")
            file.write("\n")
            file.write(f"class Day{day:02d} : public App\n")
            file.write("{\n")
            file.write("public:\n")
            file.write(f"    explicit Day{day:02d}(const CLIParams& prms) : App(\"<Title>\", prms) {{}}\n")
            file.write(f"    ~Day{day:02d}() override = default;\n")
            file.write("\n")
            file.write("    // no copy/move\n")
            file.write(f"    Day{day:02d}(const Day{day:02d}&)            = delete;\n")
            file.write(f"    Day{day:02d}(Day{day:02d}&&)                 = delete;\n")
            file.write(f"    Day{day:02d}& operator=(const Day{day:02d}&) = delete;\n")
            file.write(f"    Day{day:02d}& operator=(Day{day:02d}&&)      = delete;\n")
            file.write("\n")
            file.write("    std::string Run() override;\n")
            file.write("};\n")
            file.write("\n")
            file.write("} // namespace advent\n")

        with open(f"{source_dir}/Day{day:02d}.cpp", "w") as file:
            file.write(f"#include \"Day{day:02d}.hpp\"\n")
            file.write("\n")
            file.write("namespace advent {\n")
            file.write("\n")
            file.write(f"std::string Day{day:02d}::Run()\n")
            file.write("{\n")
            file.write(f"    return \"Day {day:02d} not yet implemented\";\n")
            file.write("}\n")
            file.write("\n")
            file.write("} // namespace advent\n")


if __name__ == "__main__":
    generate()
