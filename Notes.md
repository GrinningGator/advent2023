# Day 1

There can be overlapping number words!

For example: `eightwo`

```
one
  eight 

two
  one

three
    eight

four

five
   eight

six

seven

eight
    two
    three

nine
   eight
```


| combo       |  In Data? |
|:------------|-----------|
| `oneight`   |    yes    |
| `twone`     |    yes    |
| `eightwo`   |    yes    |
| `threeight` |    no     |
| `fiveight`  |    no     |
| `eighthree` |    no     |
| `nineight`  |    no     |


# Day 2

A *hand* is count of red, green, and blue cubes.

A *game* is a small set of *hands*. It has an ID.

Game records look like:

```
Game <ID>: [<r1> red], [<g1> green], [<b1> blue][; [<r2> red], [<g2> green], [<b2> blue]]*
```

`ParseGame()` takes in a line and returns a `Game` object.

`ParseHand()` takes in the rest of the line and parses a hand up until a `;` or the end of the line and returns a `Hand` object.


# Day 3

- Need a 2D grid of chars
  - Coordinates in graphics style (0,0) is upper left (makes more sense for how data is read in)
  - Will be loaded with a string for each row
- Need to find numbers that are adjacent to a symbol ("Periods (.) do not count as a symbol.")
    - Including diagonal
    - Possible cases:
        - up (x, y-1)
        - down (x, y+1)
        - right (x+1, y)
        - left (x-1, y)
        - up/right (x+1, y-1)
        - up/left (x-1, y-1)
        - down/right (x+1, y+1)
        - down/left (x-1, y+1)
- Loop through points
    - When encountering a symbol (anything NOT '.' or a digit), look for adjacent digits
        - If found a digit, look right and left until find ','
- Test cases
    ```
    .111.
    ....*
    
    .111.
    ...*.

    .111.
    ..*..

    .111.
    .*...

    .111.
    *....

    etc.
    ```


# Day 03 -- Retry

We need to add numbers that are **NOT** adjacent to symbols. D'oh!

So, we need to loop through cells in the grid -- left to right, top to bottom -- and when we hit a digit, we need to check if that digit is adjacent to a symbol, and only if it is not, then we add the digit to a string. Go right one and check for adjacent symbols, if a symbol is found, clear out the string. If we get to an empty cell before finding an adjacent symbol, then we add that number to a cell.

- `Day03::IsDigit()` -- keep
- `Day03::IsSymbol()` -- keep
- `Day03::GetNumber()` -- change to work left to right return 0 if any symbols are adjacent


# Day 03 -- 2nd Retry

I had it right the first time. It does want to count the number that ARE adjacent to a symbol. The problem is my first attempt -- finding the symbols then checking for adjacent numbers -- double counts a number when it is adjacent to more than one symbol.

The way to solve that is to look for numbers like Retry #1, but then count when they ARE adjacent to a symbol.


# Day 05

Maps are provided as three values: Destination start, source start, and a range length (*note that the destination comes first*).

Initial thoughts are to build a `std::map` from source to destination IDs for each step (`seed-to-soil``, `soil-to-fertilizer``, etc.). But the size of the ranges in the real input are in the billions. So, those maps would be HUGE.

I think we're going to have to store the actual start/size values and then look up the source IDs by checking if the ID is > start and ID < start + range. And then the mapped value is destination start + (ID - source start).

Going to need:

- A `Range` class
  - Destination Start (`uint64_t`)
  - Source Start (`uint64_t`)
  - Range Size (`uint64_t`)
  - A way to read this in from a line (constructor? factory method in `Day05`?)
- A `Map` class
    - A collection (`std::vector`?) of `Range`s
    - A function (`At()`?) to map Source ID to Destination ID
- A function to parse the file
    - Read labels and dispatch appropriately 
- A function to parse a collection of numbers (seed ID's and map entries)

Part 2 needs a much more efficient method... the numbers are HUGE


# Day 6

Seems there should be a formula. 

There's a time limit $t$. There are two phases: time holding down the button $h$, and time with the boat moving $m$.

```math
t = h + m
```

The boat moves at speed $h$ for time $m$, so the distance traveled $d$ is:

```math
d = h m
```

```math
d = h \left( t-h \right)
```

Which is a quadratic equation in $h$.

We want to know what (integer) $h$ values break the record distance $d'$, or

```math
h \in \mathbb{Z+} | h \left( t-h \right) > d'
```


This happens when $h \left( t-h \right) - d' > 0$

I think if we set the left hand side equal to 0 and solve for the roots of the quadratic equation, we'll get the start and the end points. We'll need to round the lower one up and the upper one down to get integers.

```math
h \left( t-h \right) - d' = 0
```

```math
-h^2 + th - d' = 0
```

Applying the quadratic formula with $a = -1$, $b = t$, and $c = -d'$


```math
h = \frac{-t \pm \sqrt{t^2 - 4 \left(-1\right) \left(-d'\right)}}{2 \left(-1\right)}
```

```math
h = \frac{-t \pm \sqrt{t^2 - 4 d'}}{-2}
```


## Example

$t = 7$, $d' = 9$

```math
h = \frac{-7 \pm \sqrt{7^2 - 4 \cdot 9}}{-2}
```

```math
h = \frac{-7 \pm \sqrt{13}}{-2}
```

```math
h = \frac{-7 \pm 3.60555}{-2}
```

```math
h = \frac{-7 + 3.60555}{-2} = 1.697225
```

```math
h = \frac{-7 - 3.60555}{-2} = 5.302775
```
