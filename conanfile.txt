[requires]
argh/1.3.2
ctre/3.8.1
fmt/10.0.0
range-v3/0.12.0


[build_requires]
catch2/3.5.0

[generators]
cmake_find_package
