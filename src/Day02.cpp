#include "Day02.hpp"

#include <ctre.hpp>
#include <fmt/format.h>


namespace advent {

std::string Day02::Run()
{
    const advent::RGB contents{12, 13, 14};

    std::ifstream inf = OpenFile();

    std::string line;
    int         sum = 0;
    while(std::getline(inf, line))
    {
        const Game game = ParseGame(line);

        if(_prms.part == CLIParams::Part::PART1)
        {
            if(IsGamePossible(game, contents))
            {
                sum += game.id;
            }
        }
        else
        {
            sum += Power(MinCubes(game));
        }
    }

    return fmt::format("{}", sum);
}


Game Day02::ParseGame(std::string_view line) const
{
    static constexpr auto game_id_regex = ctll::fixed_string{"Game (\\d+):"};
    static constexpr auto hand_regex    = ctll::fixed_string{"(?<=[:;])(.+?)((?=[;\\n])|$)"};

    Game game;

    const auto match = ctre::search<game_id_regex>(line);

    game.id = match.get<1>().to_number();

    for(const auto& hand : ctre::range<hand_regex>(line))
    {
        game.hands.emplace_back(ParseHand(hand.get<1>().to_view()));
    }

    return game;
}


RGB Day02::ParseHand(std::string_view line) const
{
    static constexpr auto split_chars = ctll::fixed_string{"(\\s|;|,)"};

    int red        = 0;
    int green      = 0;
    int blue       = 0;
    int last_value = 0;

    for(const auto match : ctre::split<split_chars>(line))
    {
        const std::string_view color = match.to_view();

        if(color.length() > 0)
        {
            const int value = match.to_number();

            if(value > 0)
            {
                last_value = value;
            }
            else
            {
                if(color == "red")
                {
                    red = last_value;
                }
                else if(color == "blue")
                {
                    blue = last_value;
                }
                else if(color == "green")
                {
                    green = last_value;
                }
                else
                {
                    throw std::runtime_error(
                        fmt::format("Parse Error: unrecognized color '{}'", color)
                    );
                }
            }
        }
    }

    return {red, green, blue};
}


bool Day02::IsGamePossible(const Game& game, const RGB& contents) const
{
    for(const RGB hand : game.hands)
    {
        if(hand.red > contents.red || hand.green > contents.green || hand.blue > contents.blue)
        {
            return false;
        }
    }

    return true;
}


RGB Day02::MinCubes(const Game& game) const
{
    int red   = -1;
    int green = -1;
    int blue  = -1;

    for(const RGB hand : game.hands)
    {
        if(hand.red > red)
        {
            red = hand.red;
        }

        if(hand.green > green)
        {
            green = hand.green;
        }

        if(hand.blue > blue)
        {
            blue = hand.blue;
        }
    }

    return {red, green, blue};
}

} // namespace advent
