#pragma once

#include "advent.hpp"

namespace advent {

class Day15 : public App
{
public:
    explicit Day15(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day15() override = default;

    // no copy/move
    Day15(const Day15&)            = delete;
    Day15(Day15&&)                 = delete;
    Day15& operator=(const Day15&) = delete;
    Day15& operator=(Day15&&)      = delete;

    std::string Run() override;
};

} // namespace advent
