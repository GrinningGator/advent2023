#pragma once

#include "advent.hpp"

namespace advent {

class Day23 : public App
{
public:
    explicit Day23(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day23() override = default;

    // no copy/move
    Day23(const Day23&)            = delete;
    Day23(Day23&&)                 = delete;
    Day23& operator=(const Day23&) = delete;
    Day23& operator=(Day23&&)      = delete;

    std::string Run() override;
};

} // namespace advent
