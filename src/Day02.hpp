#pragma once

#include <vector>

#include "advent.hpp"

namespace advent {

struct RGB
{
    int red{0};
    int green{0};
    int blue{0};
};

struct Game
{
    int              id{0};
    std::vector<RGB> hands;
};


class Day02 : public App
{
public:
    explicit Day02(const CLIParams& prms) : App("Cube Conundrum", prms) {}
    ~Day02() override = default;

    // no copy/move
    Day02(const Day02&)            = delete;
    Day02(Day02&&)                 = delete;
    Day02& operator=(const Day02&) = delete;
    Day02& operator=(Day02&&)      = delete;

    std::string Run() override;


    [[nodiscard]] Game ParseGame(std::string_view line) const;
    [[nodiscard]] RGB  ParseHand(std::string_view line) const;

    [[nodiscard]] bool IsGamePossible(const Game& game, const RGB& contents) const;

    [[nodiscard]] int Power(const RGB& rgb) const { return rgb.red * rgb.green * rgb.blue; }

    [[nodiscard]] RGB MinCubes(const Game& game) const;
};

} // namespace advent
