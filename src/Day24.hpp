#pragma once

#include "advent.hpp"

namespace advent {

class Day24 : public App
{
public:
    explicit Day24(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day24() override = default;

    // no copy/move
    Day24(const Day24&)            = delete;
    Day24(Day24&&)                 = delete;
    Day24& operator=(const Day24&) = delete;
    Day24& operator=(Day24&&)      = delete;

    std::string Run() override;
};

} // namespace advent
