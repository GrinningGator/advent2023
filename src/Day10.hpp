#pragma once

#include "advent.hpp"

namespace advent {

class Day10 : public App
{
public:
    explicit Day10(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day10() override = default;

    // no copy/move
    Day10(const Day10&)            = delete;
    Day10(Day10&&)                 = delete;
    Day10& operator=(const Day10&) = delete;
    Day10& operator=(Day10&&)      = delete;

    std::string Run() override;
};

} // namespace advent
