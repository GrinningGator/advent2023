#pragma once

#include "advent.hpp"

namespace advent {

class Day18 : public App
{
public:
    explicit Day18(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day18() override = default;

    // no copy/move
    Day18(const Day18&)            = delete;
    Day18(Day18&&)                 = delete;
    Day18& operator=(const Day18&) = delete;
    Day18& operator=(Day18&&)      = delete;

    std::string Run() override;
};

} // namespace advent
