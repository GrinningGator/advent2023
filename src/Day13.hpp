#pragma once

#include "advent.hpp"

namespace advent {

class Day13 : public App
{
public:
    explicit Day13(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day13() override = default;

    // no copy/move
    Day13(const Day13&)            = delete;
    Day13(Day13&&)                 = delete;
    Day13& operator=(const Day13&) = delete;
    Day13& operator=(Day13&&)      = delete;

    std::string Run() override;
};

} // namespace advent
