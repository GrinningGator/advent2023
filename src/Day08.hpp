#pragma once

#include <utility>

#include "advent.hpp"

namespace advent {

class Day08 : public App
{
public:
    struct LRNodes
    {
        std::string left;
        std::string right;
    };

    explicit Day08(const CLIParams& prms) : App("Haunted Wasteland", prms) {}
    ~Day08() override = default;

    // no copy/move
    Day08(const Day08&)            = delete;
    Day08(Day08&&)                 = delete;
    Day08& operator=(const Day08&) = delete;
    Day08& operator=(Day08&&)      = delete;

    std::string Run() override;

    [[nodiscard]] std::pair<std::string, LRNodes> ParseNode(std::string_view line) const;
};

} // namespace advent
