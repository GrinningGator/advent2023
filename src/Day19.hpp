#pragma once

#include "advent.hpp"

namespace advent {

class Day19 : public App
{
public:
    explicit Day19(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day19() override = default;

    // no copy/move
    Day19(const Day19&)            = delete;
    Day19(Day19&&)                 = delete;
    Day19& operator=(const Day19&) = delete;
    Day19& operator=(Day19&&)      = delete;

    std::string Run() override;
};

} // namespace advent
