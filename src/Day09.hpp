#pragma once

#include "advent.hpp"

namespace advent {

class Day09 : public App
{
public:
    explicit Day09(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day09() override = default;

    // no copy/move
    Day09(const Day09&)            = delete;
    Day09(Day09&&)                 = delete;
    Day09& operator=(const Day09&) = delete;
    Day09& operator=(Day09&&)      = delete;

    std::string Run() override;
};

} // namespace advent
