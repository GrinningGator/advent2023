#include "Day03.hpp"

#include <cctype>


namespace advent {

std::string Day03::Run()
{
    std::ifstream inf = OpenFile();

    LoadGrid(inf);

    const int sum = Part1Sum();

    return fmt::format("{}", sum);
}

int Day03::Part1Sum() const
{
    int sum = 0;

    // Find numbers that are adjacent to symbols
    for(size_t row = 0; row < _grid.NumRows(); ++row)
    {
        for(size_t col = 0; col < _grid.NumCols(); ++col)
        {
            if(IsDigit(_grid.At(col, row)))
            {
                sum += GetNumber(col, row);

                // find end of number
                while(col < _grid.NumCols() && IsDigit(_grid.At(col, row)))
                {
                    ++col;
                }
            }
        }
    }

    return sum;
}


int Day03::Part2Sum() const
{
    return 0;
    // int sum = 0;

    // // Find gear ratios
    // for(size_t row = 0; row < _grid.NumRows(); ++row)
    // {
    //     for(size_t col = 0; col < _grid.NumCols(); ++col)
    //     {
    //         if(_grid.At(col, row) == '*')
    //         {
    //             // find adjacent number(s)
    //             // if there are two numbers, add product to sum
    //         }
    //     }
    // }

    // return sum;
}


bool Day03::IsSymbol(char chr) const
{
    if(chr == '.')
    {
        return false;
    }

    if(std::isdigit(chr) == 1)
    {
        return false;
    }

    return true;
}


bool Day03::IsDigit(char chr) const
{
    return (std::isdigit(chr) == 1);
}


void Day03::LoadGrid(std::istream& ins)
{
    std::string line;
    while(std::getline(ins, line))
    {
        _grid.LoadRow(line);
    }
}


int Day03::GetNumber(size_t col, size_t row) const
{
    if(!IsDigit(_grid.At(col, row)))
    {
        throw std::runtime_error(fmt::format("Character at Row {}, Col {} is not a digit", row, col)
        );
    }

    const size_t max_col = _grid.NumCols() - 1;
    const size_t max_row = _grid.NumRows() - 1;

    std::string num_str;
    bool        is_adjacent = false;

    while(col < _grid.NumCols() && IsDigit(_grid.At(col, row)))
    {
        // Check if adjacent to a symbol
        if((row > 0 && IsSymbol(_grid.At(col, row - 1))) ||       // up (x, y-1)
           (row < max_row && IsSymbol(_grid.At(col, row + 1))) || // down (x, y+1)
           (col < max_col && IsSymbol(_grid.At(col + 1, row))) || // right (x+1, y)
           (col > 0 && IsSymbol(_grid.At(col - 1, row))) ||       // left (x-1, y)
           (col < max_col && row > 0 && IsSymbol(_grid.At(col + 1, row - 1))
           ) ||                                                            // up/right (x+1, y-1)
           (col > 0 && row > 0 && IsSymbol(_grid.At(col - 1, row - 1))) || // up/left (x-1, y-1)
           (col < max_col && row < max_row && IsSymbol(_grid.At(col + 1, row + 1))
           ) || // down/right (x+1, y+1)
           (col > 0 && row < max_row && IsSymbol(_grid.At(col - 1, row + 1))
           )) // down/left (x-1, y+1)
        {
            is_adjacent = true;
        }

        // add digit to number
        num_str.push_back(_grid.At(col, row));
        ++col;
    }

    if(!is_adjacent)
    {
        return 0;
    }

    const int rtn = std::stoi(num_str);

    return rtn;
}


} // namespace advent
