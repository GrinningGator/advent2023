#include "Day07.hpp"

#include <algorithm>
#include <unordered_map>

#include <ctre.hpp>

#include <fmt/format.h>


namespace advent {

std::string Day07::Run()
{
    std::ifstream inf = OpenFile();

    std::vector<Hand> hands;
    std::string       line;
    while(std::getline(inf, line))
    {
        hands.emplace_back(ParseHand(line));
    }

    SortHands(hands);

    const uint64_t winnings = CalcTotalWinnings(hands);

    return fmt::format("{}", winnings);
}


Day07::HandType Day07::CalcHandType(const std::array<char, hand_size>& cards) const
{
    // count number of cards by value
    std::unordered_map<char, int> card_count;

    for(const char card : cards)
    {
        if(card_count.contains(card))
        {
            ++(card_count.at(card));
        }
        else
        {
            card_count.insert({card, 1});
        }
    }

    // check for all the same card --> five of a kind
    if(card_count.size() == 1)
    {
        return HandType::FiveOfAKind;
    }

    int min_count = hand_size;
    int max_count = 0;

    for(const auto [card, count] : card_count)
    {
        min_count = std::min(min_count, count);
        max_count = std::max(max_count, count);
    }

    // two kinds of cards: can be four of a kind or full house
    if(card_count.size() == 2)
    {
        if(min_count == 1)
        {
            return HandType::FourOfAKind;
        }
        else
        {
            return HandType::FullHouse;
        }
    }

    // 3 kinds of cards: can be three of a kind or two pair
    if(card_count.size() == 3)
    {
        if(max_count == 3)
        {
            return HandType::ThreeOfAKind;
        }
        else
        {
            return HandType::TwoPair;
        }
    }

    // 4 kinds of cards: must be one pair
    if(card_count.size() == 4)
    {
        return HandType::OnePair;
    }

    // if we get here, we have 5 unique card
    return HandType::HighCard;
}


advent::Day07::Hand Day07::ParseHand(std::string_view line) const
{
    static constexpr auto hand_regex = ctll::fixed_string{R"(([\dTJKQA]{5})\s([\d]+))"};

    const auto match = ctre::match<hand_regex>(line);


    Hand hand;

    // get cards
    auto cards_view = match.get<1>().to_view();
    for(size_t i = 0; i < hand_size; ++i)
    {
        hand.cards.at(i) = cards_view.at(i);
    }

    // get bid
    hand.bid = match.get<2>().to_number<uint64_t>();

    // get hand type
    hand.type = CalcHandType(hand.cards);


    return hand;
}


bool Day07::HandLessThan(const Hand& lhs, const Hand& rhs)
{
    static const std::unordered_map<char, int> card_rank{
        {'2', 1},
        {'3', 2},
        {'4', 3},
        {'5', 4},
        {'6', 5},
        {'7', 6},
        {'8', 7},
        {'9', 8},
        {'T', 9},
        {'J', 10},
        {'Q', 11},
        {'K', 12},
        {'A', 13}
    };


    if(lhs.type == rhs.type)
    {
        // for equal types,
        for(size_t i = 0; i < hand_size; ++i)
        {
            const int lhs_rank = card_rank.at(lhs.cards.at(i));
            const int rhs_rank = card_rank.at(rhs.cards.at(i));

            if(lhs_rank < rhs_rank)
            {
                return true;
            }
            else if(lhs_rank > rhs_rank)
            {
                return false;
            }
        }

        // all cards are equal
        return false;
    }

    // return > because enum is listed best to worst
    return lhs.type > rhs.type;
}


void Day07::SortHands(std::vector<Hand>& hands) const
{
    std::sort(hands.begin(), hands.end(), Day07::HandLessThan);
}

uint64_t Day07::CalcTotalWinnings(const std::vector<Hand>& hands) const
{
    uint64_t sum = 0ull;

    for(size_t i = 0; i < hands.size(); ++i)
    {
        sum += (i + 1) * hands.at(i).bid;
    }

    return sum;
}

} // namespace advent
