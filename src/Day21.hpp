#pragma once

#include "advent.hpp"

namespace advent {

class Day21 : public App
{
public:
    explicit Day21(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day21() override = default;

    // no copy/move
    Day21(const Day21&)            = delete;
    Day21(Day21&&)                 = delete;
    Day21& operator=(const Day21&) = delete;
    Day21& operator=(Day21&&)      = delete;

    std::string Run() override;
};

} // namespace advent
