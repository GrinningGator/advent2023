#pragma once

#include "advent.hpp"

#include <string_view>
#include <vector>


namespace advent {

class Day06 : public App
{
public:
    explicit Day06(const CLIParams& prms) : App("Wait For It", prms) {}
    ~Day06() override = default;

    // no copy/move
    Day06(const Day06&)            = delete;
    Day06(Day06&&)                 = delete;
    Day06& operator=(const Day06&) = delete;
    Day06& operator=(Day06&&)      = delete;

    std::string Run() override;

    [[nodiscard]] uint64_t NumRecords(uint64_t raceTime, uint64_t recordDistance) const;

    [[nodiscard]] std::vector<uint64_t> ParseInts(std::string_view line) const;
};

} // namespace advent
