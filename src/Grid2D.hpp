#pragma once

#include <exception>
#include <string_view>
#include <vector>

#include <fmt/format.h>

namespace advent {

class Grid2D
{
public:
    void LoadRow(std::string_view line)
    {
        // set number of columns
        if(_nCols == 0)
        {
            if(_nCols > 0 && _nCols != line.length())
            {
                throw std::runtime_error(fmt::format(
                    "Grid2D; Existing # of column: {}; New row has {} columns.",
                    _nCols,
                    line.length()
                ));
            }

            _nCols = line.length();
        }

        // count rows
        ++_nRows;

        for(const char chr : line)
        {
            _data.push_back(chr);
        }
    }

    [[nodiscard]] size_t NumCols() const { return _nCols; }
    [[nodiscard]] size_t NumRows() const { return _nRows; }

    [[nodiscard]] char At(size_t col, size_t row) const { return _data.at((row * _nCols) + col); }

private:
    std::vector<char> _data;
    size_t            _nCols{0};
    size_t            _nRows{0};
};

} // namespace advent
