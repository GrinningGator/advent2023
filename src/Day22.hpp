#pragma once

#include "advent.hpp"

namespace advent {

class Day22 : public App
{
public:
    explicit Day22(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day22() override = default;

    // no copy/move
    Day22(const Day22&)            = delete;
    Day22(Day22&&)                 = delete;
    Day22& operator=(const Day22&) = delete;
    Day22& operator=(Day22&&)      = delete;

    std::string Run() override;
};

} // namespace advent
