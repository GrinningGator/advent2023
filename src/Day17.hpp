#pragma once

#include "advent.hpp"

namespace advent {

class Day17 : public App
{
public:
    explicit Day17(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day17() override = default;

    // no copy/move
    Day17(const Day17&)            = delete;
    Day17(Day17&&)                 = delete;
    Day17& operator=(const Day17&) = delete;
    Day17& operator=(Day17&&)      = delete;

    std::string Run() override;
};

} // namespace advent
