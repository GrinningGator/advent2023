#pragma once

#include <string_view>

#include "advent.hpp"

namespace advent {

class Day01 : public App
{
public:
    explicit Day01(const CLIParams& prms) : App("Trebuchet?!", prms) {}
    ~Day01() override = default;

    // no copy/move
    Day01(const Day01&)            = delete;
    Day01(Day01&&)                 = delete;
    Day01& operator=(const Day01&) = delete;
    Day01& operator=(Day01&&)      = delete;

    std::string Run() override;

    [[nodiscard]] int ParseDigits(std::string_view line) const;
    [[nodiscard]] int ParseNumbers(std::string_view line) const;

    [[nodiscard]] int CalcCalibValue(std::istream& ins) const;
};

} // namespace advent
