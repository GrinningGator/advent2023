#include "Day06.hpp"

#include <ctre.hpp>
#include <fmt/format.h>

#include <cmath>


namespace advent {

std::string Day06::Run()
{
    std::ifstream inf = OpenFile();

    std::string line;

    std::getline(inf, line);
    const std::vector<uint64_t> time = ParseInts(line);

    std::getline(inf, line);
    const std::vector<uint64_t> distance = ParseInts(line);

    uint64_t accum = (_prms.part == CLIParams::Part::PART1 ? 1 : 0);

    for(size_t i = 0; i < time.size(); ++i)
    {
        const uint64_t count = NumRecords(time.at(i), distance.at(i));

        if(_prms.part == CLIParams::Part::PART1)
        {
            accum *= count;
        }
        else
        {
            accum += count;
        }
    }

    return fmt::format("{}", accum);
}


uint64_t Day06::NumRecords(uint64_t raceTime, uint64_t recordDistance) const
{
    static constexpr double eps = 0.00001;

    const double dist_dbl = static_cast<double>(recordDistance) + eps;

    const double discrimiant = static_cast<double>(raceTime * raceTime) - 4.0 * dist_dbl;
    const double sqrt_d      = std::sqrt(discrimiant);

    const double min_root = (static_cast<double>(raceTime) - sqrt_d) / (2.0);
    const double max_root = (static_cast<double>(raceTime) + sqrt_d) / (2.0);

    const auto min_root_round = static_cast<uint64_t>(std::ceil(min_root));
    const auto max_root_round = static_cast<uint64_t>(max_root);

    return max_root_round - min_root_round + 1ull;
}


std::vector<uint64_t> Day06::ParseInts(std::string_view line) const
{
    static constexpr auto int_regex = ctll::fixed_string{"([\\d]+)"};

    std::vector<uint64_t> values;

    for(auto match : ctre::range<int_regex>(line))
    {
        const auto val = match.get<1>().to_number<uint64_t>();

        values.push_back(val);
    }

    return values;
}

} // namespace advent
