#pragma once

#include <fstream>
#include <memory>
#include <string>
#include <utility>

namespace advent {

struct CLIParams
{
    CLIParams() = default;
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
    CLIParams(int argc, const char* argv[]);


    enum class Day
    {
        UNDEF,
        DAY01,
        DAY02,
        DAY03,
        DAY04,
        DAY05,
        DAY06,
        DAY07,
        DAY08,
        DAY09,
        DAY10,
        DAY11,
        DAY12,
        DAY13,
        DAY14,
        DAY15,
        DAY16,
        DAY17,
        DAY18,
        DAY19,
        DAY20,
        DAY21,
        DAY22,
        DAY23,
        DAY24,
        DAY25
    };

    enum class Part
    {
        PART1,
        PART2
    };

    Day         day{Day::UNDEF};
    std::string inFile;
    Part        part{Part::PART1};
};


class App
{
public:
    explicit App(std::string name, CLIParams prms) : _name{std::move(name)}, _prms{std::move(prms)}
    {
    }

    virtual ~App() = default;

    // no copy/move
    App(const App&)            = delete;
    App(App&&)                 = delete;
    App& operator=(const App&) = delete;
    App& operator=(App&&)      = delete;

    [[nodiscard]] const std::string& Name() const { return _name; }

    virtual std::string Run() = 0;

    [[nodiscard]] static std::unique_ptr<App> Create(const CLIParams& prms);

protected:
    [[nodiscard]] std::ifstream OpenFile() const
    {
        std::ifstream inf(_prms.inFile);

        if(!inf.is_open())
        {
            throw std::runtime_error("Could not open \"" + _prms.inFile + "\"");
        }

        return inf;
    }

    std::string _name;
    CLIParams   _prms;
};


} // namespace advent
