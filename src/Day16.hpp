#pragma once

#include "advent.hpp"

namespace advent {

class Day16 : public App
{
public:
    explicit Day16(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day16() override = default;

    // no copy/move
    Day16(const Day16&)            = delete;
    Day16(Day16&&)                 = delete;
    Day16& operator=(const Day16&) = delete;
    Day16& operator=(Day16&&)      = delete;

    std::string Run() override;
};

} // namespace advent
