#pragma once

#include <set>
#include <string_view>
#include <utility>

#include "advent.hpp"


namespace advent {

class Day04 : public App
{
public:
    explicit Day04(const CLIParams& prms) : App("Scratchcards", prms) {}
    ~Day04() override = default;

    // no copy/move
    Day04(const Day04&)            = delete;
    Day04(Day04&&)                 = delete;
    Day04& operator=(const Day04&) = delete;
    Day04& operator=(Day04&&)      = delete;

    std::string Run() override;

    [[nodiscard]] std::pair<std::set<int>, std::set<int>> SplitLine(std::string_view line) const;
    [[nodiscard]] std::set<int>
    Intersect(const std::set<int>& setA, const std::set<int>& setB) const;
};

} // namespace advent
