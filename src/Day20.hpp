#pragma once

#include "advent.hpp"

namespace advent {

class Day20 : public App
{
public:
    explicit Day20(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day20() override = default;

    // no copy/move
    Day20(const Day20&)            = delete;
    Day20(Day20&&)                 = delete;
    Day20& operator=(const Day20&) = delete;
    Day20& operator=(Day20&&)      = delete;

    std::string Run() override;
};

} // namespace advent
