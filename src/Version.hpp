////////////////////////////////////////////////////////////////////////////////////////////////////
// AUTO GENERATED -- DO NOT EDIT
// (Change project version number in main CMakeLists.txt file and reconfigure)
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma once

constexpr unsigned int ADVENT2023_VERS_MAJOR = 0;
constexpr unsigned int ADVENT2023_VERS_MINOR = 1;
constexpr unsigned int ADVENT2023_VERS_PATCH = 0;

constexpr char ADVENT2023_VERS[] = "0.1.0";
