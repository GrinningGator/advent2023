#include "Day04.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <vector>

#include <fmt/format.h>


namespace advent {

std::string Day04::Run()
{
    std::ifstream inf = OpenFile();

    uint64_t         sum       = 0;
    const size_t     num_cards = 201;
    std::vector<int> count(num_cards, 1);

    std::string line;
    size_t      index = 0;
    while(std::getline(inf, line))
    {
        const auto [winning, tickets] = SplitLine(line);

        const auto intersect = Intersect(winning, tickets);

        if(_prms.part == CLIParams::Part::PART1)
        {
            if(!intersect.empty())
            {
                sum += (1ul << (intersect.size() - 1));
            }
        }
        else
        {
            for(size_t i = 1; i <= intersect.size(); ++i)
            {
                count.at(index + i) += count.at(index);
            }
        }

        ++index;
    }

    if(_prms.part == CLIParams::Part::PART2)
    {
        sum = std::accumulate(count.begin(), std::next(count.begin(), (long)index), 0ul);
    }

    return fmt::format("{}", sum);
}

std::pair<std::set<int>, std::set<int>> Day04::SplitLine(std::string_view line) const
{
    std::set<int> winning;
    std::set<int> tickets;

    size_t pos = 0;

    // skip to end of "Card X:"
    while(line.at(pos) != ':')
    {
        ++pos;
    }

    // Skip ": "
    pos += 2;

    while(line.at(pos) != '|')
    {
        if(std::isdigit(line.at(pos)) == 1)
        {
            const size_t start = pos;

            while(std::isdigit(line.at(pos)) == 1)
            {
                ++pos;
            }

            const auto num_str = std::string(line.substr(start, pos - start));

            winning.insert(std::stoi(num_str));
        }

        ++pos;
    }


    while(pos < line.length())
    {
        if(std::isdigit(line.at(pos)) == 1)
        {
            const size_t start = pos;

            while(pos < line.length() && std::isdigit(line.at(pos)) == 1)
            {
                ++pos;
            }

            const auto num_str = std::string(line.substr(start, pos - start));

            tickets.insert(std::stoi(num_str));
        }

        ++pos;
    }


    return {winning, tickets};
}


std::set<int> Day04::Intersect(const std::set<int>& setA, const std::set<int>& setB) const
{
    std::set<int> rtn;

    std::set_intersection(
        setA.begin(), setA.end(), setB.begin(), setB.end(), std::inserter(rtn, rtn.begin())
    );

    return rtn;
}

} // namespace advent
