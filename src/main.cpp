#include "Version.hpp"
#include "advent.hpp"

#include <string>

#include <fmt/format.h>


int main(int argc, const char* argv[])
{
    fmt::print("Advent of Code 2022 - Version {}\n", ADVENT2023_VERS);

    const advent::CLIParams prms(argc, argv);

    auto app = advent::App::Create(prms);

    const std::string result = app->Run();

    fmt::print("\n\nAdvent of Code 2023:\n");
    fmt::print("  Day:        {:02} - \"{}\"\n", static_cast<int>(prms.day), app->Name());
    fmt::print("  Input File: {}\n", prms.inFile);
    fmt::print("  Part:       {}\n\n", static_cast<int>(prms.part) + 1);
    fmt::print("  RESULT:     {}\n", result);
    fmt::print("\n");

    return 0;
}
