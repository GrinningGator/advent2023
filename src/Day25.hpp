#pragma once

#include "advent.hpp"

namespace advent {

class Day25 : public App
{
public:
    explicit Day25(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day25() override = default;

    // no copy/move
    Day25(const Day25&)            = delete;
    Day25(Day25&&)                 = delete;
    Day25& operator=(const Day25&) = delete;
    Day25& operator=(Day25&&)      = delete;

    std::string Run() override;
};

} // namespace advent
