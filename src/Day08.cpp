#include "Day08.hpp"

#include <algorithm>
#include <numeric>
#include <unordered_map>
#include <vector>

#include <ctre.hpp>

#include <fmt/format.h>


namespace advent {

std::string Day08::Run()
{
    std::ifstream inf = OpenFile();

    std::string line;

    // get first line (L/R instructions)
    std::getline(inf, line);

    const std::string instructions = line;

    // skip a line
    std::getline(inf, line);


    // rest of file contains nodes
    std::unordered_map<std::string, LRNodes> map_nodes;
    while(std::getline(inf, line))
    {
        const auto [name, nodes] = ParseNode(line);

        map_nodes.insert_or_assign(name, nodes);
    }


    // process instructions
    std::vector<std::string> curr;

    if(_prms.part == CLIParams::Part::PART1)
    {
        curr.emplace_back("AAA");
    }
    else
    {
        for(const auto& [name, nodes] : map_nodes)
        {
            if(name.ends_with('A'))
            {
                curr.push_back(name);
            }
        }
    }

    std::vector<uint64_t> counts;

    for(auto currn : curr)
    {
        uint64_t count = 0;


        while(!currn.ends_with('Z'))
        {
            for(const char dir : instructions)
            {
                ++count;

                currn = (dir == 'L' ? map_nodes.at(currn).left : map_nodes.at(currn).right);

                if(currn.ends_with('Z'))
                {
                    break;
                }
            }
        }

        counts.push_back(count);
    }

    const uint64_t lcm =
        std::accumulate(counts.begin(), counts.end(), counts.at(0), std::lcm<uint64_t, uint64_t>);


    return fmt::format("{}", lcm);
}


std::pair<std::string, Day08::LRNodes> Day08::ParseNode(std::string_view line) const
{
    static constexpr auto node_regex =
        ctll::fixed_string{R"(([A-Z\d]{3}).+?\(([A-Z\d]{3}).+?([A-Z\d]{3})\))"};

    const auto match = ctre::match<node_regex>(line);

    const std::string name  = match.get<1>().to_string();
    const std::string left  = match.get<2>().to_string();
    const std::string right = match.get<3>().to_string();

    return {name, {left, right}};
}

} // namespace advent
