#include "Day05.hpp"

#include <string>
#include <string_view>

#include <ctre.hpp>
#include <fmt/format.h>


namespace advent {

std::string Day05::Run()
{
    std::ifstream inf = OpenFile();
    ParseInput(inf);

    uint64_t min = std::numeric_limits<uint64_t>::max();

    fmt::println("Prcessing {} seeds...", _seeds.size());

    if(_prms.part == CLIParams::Part::PART1)
    {
        for(const uint64_t seed : _seeds)
        {
            const uint64_t location = LocationFromSeed(seed);

            min = std::min(min, location);
        }
    }
    else
    {
        min = ProcessSeedRanges(_seeds);
    }

    return fmt::format("{}", min);
}


std::vector<uint64_t> Day05::ParseNumbers(std::string_view line) const
{
    std::vector<uint64_t> values;

    size_t pos = 0;

    while(pos < line.length())
    {
        while(pos < line.length() && std::isdigit(line.at(pos)) == 0)
        {
            ++pos;
        }

        const size_t start = pos;

        while(pos < line.length() && std::isdigit(line.at(pos)) == 1)
        {
            ++pos;
        }

        const std::string num_str{line.substr(start, pos - start)};

        values.push_back(std::stoull(num_str));
    }

    return values;
}


Range Day05::ParseRange(std::string_view line) const
{
    const auto values = ParseNumbers(line);

    return {values.at(0), values.at(1), values.at(2)};
}


std::vector<Range> Day05::ParseAllRanges(std::istream& ins) const
{
    std::string        line;
    std::vector<Range> ranges;

    while(std::getline(ins, line))
    {
        if(line.length() == 0)
        {
            break;
        }

        ranges.emplace_back(ParseRange(line));
    }

    return ranges;
}


void Day05::ParseInput(std::istream& ins)
{
    static constexpr auto    label_regex = ctll::fixed_string{"([a-z\\- ]+)"};
    static const std::string seed_label  = "seeds";

    std::string line;
    while(std::getline(ins, line))
    {
        if(line.length() > 0)
        {
            const auto match = ctre::search<label_regex>(line);

            const auto label = match.get<1>().to_string();

            if(label == seed_label)
            {
                _seeds = ParseNumbers(line.substr(seed_label.length()));
            }
            else
            {
                const std::vector<Range> ranges = ParseAllRanges(ins);

                if(label == "seed-to-soil map")
                {
                    _seedToSoil = std::make_unique<Map>(ranges);
                }
                else if(label == "soil-to-fertilizer map")
                {
                    _soilToFert = std::make_unique<Map>(ranges);
                }
                else if(label == "fertilizer-to-water map")
                {
                    _fertToWater = std::make_unique<Map>(ranges);
                }
                else if(label == "water-to-light map")
                {
                    _waterToLight = std::make_unique<Map>(ranges);
                }
                else if(label == "light-to-temperature map")
                {
                    _lightToTemp = std::make_unique<Map>(ranges);
                }
                else if(label == "temperature-to-humidity map")
                {
                    _tempToHumid = std::make_unique<Map>(ranges);
                }
                else if(label == "humidity-to-location map")
                {
                    _humidToLocation = std::make_unique<Map>(ranges);
                }
            }
        }
    }
}


uint64_t Day05::LocationFromSeed(uint64_t seed) const
{
    const uint64_t soil     = _seedToSoil->At(seed);
    const uint64_t fert     = _soilToFert->At(soil);
    const uint64_t water    = _fertToWater->At(fert);
    const uint64_t light    = _waterToLight->At(water);
    const uint64_t temp     = _lightToTemp->At(light);
    const uint64_t humid    = _tempToHumid->At(temp);
    const uint64_t location = _humidToLocation->At(humid);

    return location;
}


uint64_t Day05::ProcessSeedRanges(const std::vector<uint64_t>& ranges) const
{
    fmt::println("Seed ranges vector size {}", ranges.size());

    uint64_t min = std::numeric_limits<uint64_t>::max();

    for(size_t i = 0; i < ranges.size(); ++i)
    {
        if(i % 2 == 1)
        {
            const uint64_t start = ranges.at(i - 1);
            const uint64_t size  = ranges.at(i);

            fmt::println("Seed start {}; size {}", start, size);

            for(uint64_t j = start; j < start + size; ++j)
            {
                const uint64_t location = LocationFromSeed(j);

                min = std::min(min, location);
            }
        }
    }

    return min;
}


} // namespace advent
