#pragma once

#include <memory>
#include <utility>
#include <vector>

#include "advent.hpp"


namespace advent {

struct Range
{
    uint64_t destStart{0};
    uint64_t sourceStart{0};
    uint64_t range{0};
};

class Map
{
public:
    explicit Map(std::vector<Range> ranges) : _ranges(std::move(ranges)) {}

    [[nodiscard]] uint64_t At(uint64_t source) const
    {
        for(const auto& range : _ranges)
        {
            if(source >= range.sourceStart && source < range.sourceStart + range.range)
            {
                const uint64_t offset = source - range.sourceStart;

                return range.destStart + offset;
            }
        }

        return source;
    }

private:
    std::vector<Range> _ranges;
};


class Day05 : public App
{
public:
    explicit Day05(const CLIParams& prms) : App("If You Give A Seed A Fertilizer", prms) {}
    ~Day05() override = default;

    // no copy/move
    Day05(const Day05&)            = delete;
    Day05(Day05&&)                 = delete;
    Day05& operator=(const Day05&) = delete;
    Day05& operator=(Day05&&)      = delete;

    std::string Run() override;

    [[nodiscard]] std::vector<uint64_t> ParseNumbers(std::string_view line) const;

    [[nodiscard]] Range ParseRange(std::string_view line) const;

    void ParseInput(std::istream& ins);

    [[nodiscard]] uint64_t LocationFromSeed(uint64_t seed) const;

    [[nodiscard]] uint64_t ProcessSeedRanges(const std::vector<uint64_t>& ranges) const;

private:
    std::vector<uint64_t> _seeds{};
    std::unique_ptr<Map>  _seedToSoil{nullptr};
    std::unique_ptr<Map>  _soilToFert{nullptr};
    std::unique_ptr<Map>  _fertToWater{nullptr};
    std::unique_ptr<Map>  _waterToLight{nullptr};
    std::unique_ptr<Map>  _lightToTemp{nullptr};
    std::unique_ptr<Map>  _tempToHumid{nullptr};
    std::unique_ptr<Map>  _humidToLocation{nullptr};


    std::vector<Range> ParseAllRanges(std::istream& ins) const;
};

} // namespace advent
