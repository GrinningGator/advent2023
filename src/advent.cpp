#include "advent.hpp"

#include <memory>
#include <stdexcept>

#include "Day01.hpp"
#include "Day02.hpp"
#include "Day03.hpp"
#include "Day04.hpp"
#include "Day05.hpp"
#include "Day06.hpp"
#include "Day07.hpp"
#include "Day08.hpp"
#include "Day09.hpp"
#include "Day10.hpp"
#include "Day11.hpp"
#include "Day12.hpp"
#include "Day13.hpp"
#include "Day14.hpp"
#include "Day15.hpp"
#include "Day16.hpp"
#include "Day17.hpp"
#include "Day18.hpp"
#include "Day19.hpp"
#include "Day20.hpp"
#include "Day21.hpp"
#include "Day22.hpp"
#include "Day23.hpp"
#include "Day24.hpp"
#include "Day25.hpp"

#include <argh.h>

namespace advent {

// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays)
CLIParams::CLIParams(int argc, const char* argv[])
{
    const argh::parser cli(argc, argv);

    // day
    int arg1 = 0;
    cli(1) >> arg1;

    day = static_cast<Day>(static_cast<int>(Day::UNDEF) + arg1);

    // input file
    cli(2) >> inFile;

    // part1 or 2?
    part = (cli["part2"] ? Part::PART2 : Part::PART1);
}

std::unique_ptr<App> App::Create(const CLIParams& prms)
{
    switch(prms.day)
    {
        using Day = CLIParams::Day;

        case Day::DAY01: return std::make_unique<Day01>(prms);
        case Day::DAY02: return std::make_unique<Day02>(prms);
        case Day::DAY03: return std::make_unique<Day03>(prms);
        case Day::DAY04: return std::make_unique<Day04>(prms);
        case Day::DAY05: return std::make_unique<Day05>(prms);
        case Day::DAY06: return std::make_unique<Day06>(prms);
        case Day::DAY07: return std::make_unique<Day07>(prms);
        case Day::DAY08: return std::make_unique<Day08>(prms);
        case Day::DAY09: return std::make_unique<Day09>(prms);
        case Day::DAY10: return std::make_unique<Day10>(prms);
        case Day::DAY11: return std::make_unique<Day11>(prms);
        case Day::DAY12: return std::make_unique<Day12>(prms);
        case Day::DAY13: return std::make_unique<Day13>(prms);
        case Day::DAY14: return std::make_unique<Day14>(prms);
        case Day::DAY15: return std::make_unique<Day15>(prms);
        case Day::DAY16: return std::make_unique<Day16>(prms);
        case Day::DAY17: return std::make_unique<Day17>(prms);
        case Day::DAY18: return std::make_unique<Day18>(prms);
        case Day::DAY19: return std::make_unique<Day19>(prms);
        case Day::DAY20: return std::make_unique<Day20>(prms);
        case Day::DAY21: return std::make_unique<Day21>(prms);
        case Day::DAY22: return std::make_unique<Day22>(prms);
        case Day::DAY23: return std::make_unique<Day23>(prms);
        case Day::DAY24: return std::make_unique<Day24>(prms);
        case Day::DAY25: return std::make_unique<Day25>(prms);

        default: throw std::runtime_error("Invalid day.");
    }
}

} // namespace advent
