#pragma once

#include "advent.hpp"

namespace advent {

class Day12 : public App
{
public:
    explicit Day12(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day12() override = default;

    // no copy/move
    Day12(const Day12&)            = delete;
    Day12(Day12&&)                 = delete;
    Day12& operator=(const Day12&) = delete;
    Day12& operator=(Day12&&)      = delete;

    std::string Run() override;
};

} // namespace advent
