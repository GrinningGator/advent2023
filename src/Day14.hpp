#pragma once

#include "advent.hpp"

namespace advent {

class Day14 : public App
{
public:
    explicit Day14(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day14() override = default;

    // no copy/move
    Day14(const Day14&)            = delete;
    Day14(Day14&&)                 = delete;
    Day14& operator=(const Day14&) = delete;
    Day14& operator=(Day14&&)      = delete;

    std::string Run() override;
};

} // namespace advent
