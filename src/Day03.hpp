#pragma once

#include "Grid2D.hpp"
#include "advent.hpp"

namespace advent {

class Day03 : public App
{
public:
    explicit Day03(const CLIParams& prms) : App("Gear Ratios", prms) {}
    ~Day03() override = default;

    // no copy/move
    Day03(const Day03&)            = delete;
    Day03(Day03&&)                 = delete;
    Day03& operator=(const Day03&) = delete;
    Day03& operator=(Day03&&)      = delete;

    std::string Run() override;

    [[nodiscard]] bool IsSymbol(char chr) const;
    [[nodiscard]] bool IsDigit(char chr) const;

    void LoadGrid(std::istream& ins);

    [[nodiscard]] const Grid2D& Grid() const { return _grid; }

    [[nodiscard]] int GetNumber(size_t col, size_t row) const;

    [[nodiscard]] int Part1Sum() const;
    [[nodiscard]] int Part2Sum() const;

private:
    Grid2D _grid;
};

} // namespace advent
