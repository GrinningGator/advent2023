#pragma once

#include "advent.hpp"

namespace advent {

class Day11 : public App
{
public:
    explicit Day11(const CLIParams& prms) : App("<Title>", prms) {}
    ~Day11() override = default;

    // no copy/move
    Day11(const Day11&)            = delete;
    Day11(Day11&&)                 = delete;
    Day11& operator=(const Day11&) = delete;
    Day11& operator=(Day11&&)      = delete;

    std::string Run() override;
};

} // namespace advent
