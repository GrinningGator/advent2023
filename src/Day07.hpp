#pragma once

#include <array>
#include <vector>

#include "advent.hpp"

namespace advent {

class Day07 : public App
{
public:
    enum class HandType
    {
        FiveOfAKind,
        FourOfAKind,
        FullHouse,
        ThreeOfAKind,
        TwoPair,
        OnePair,
        HighCard,
        Unknown
    };

    static constexpr size_t hand_size = 5;
    struct Hand
    {
        std::array<char, hand_size> cards{};
        uint64_t                    bid{0};
        HandType                    type{HandType::Unknown};
    };

    explicit Day07(const CLIParams& prms) : App("Camel Cards", prms) {}
    ~Day07() override = default;

    // no copy/move
    Day07(const Day07&)            = delete;
    Day07(Day07&&)                 = delete;
    Day07& operator=(const Day07&) = delete;
    Day07& operator=(Day07&&)      = delete;

    std::string Run() override;

    [[nodiscard]] HandType CalcHandType(const std::array<char, hand_size>& cards) const;
    [[nodiscard]] Hand     ParseHand(std::string_view line) const;
    static bool            HandLessThan(const Hand& lhs, const Hand& rhs);
    void                   SortHands(std::vector<Hand>& hands) const;
    uint64_t               CalcTotalWinnings(const std::vector<Hand>& hands) const;
};

} // namespace advent
