#include "Day01.hpp"

#include <unordered_map>

#include <ctre.hpp>
#include <fmt/format.h>

namespace advent {

std::string Day01::Run()
{
    std::ifstream inf = OpenFile();

    const int rtn_val = CalcCalibValue(inf);

    return fmt::format("{}", rtn_val);
}

int Day01::ParseDigits(std::string_view line) const
{
    static constexpr auto pattern = ctll::fixed_string{"([0-9])"};

    int first = -1;
    int last  = -1;

    for(auto match : ctre::range<pattern>(line))
    {
        const int num = match.to_number();

        if(first < 0)
        {
            first = num;
        }

        last = num;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
    return first * 10 + last;
}


int Day01::ParseNumbers(std::string_view line) const
{
    static constexpr auto pattern = ctll::fixed_string{
        "(oneight|twone|eightwo|one|two|three|four|five|six|seven|eight|nine|[0-9])"
    };

    static const std::unordered_map<std::string_view, int> str_to_num{
        {"one", 1},
        {"two", 2},
        {"three", 3},
        {"four", 4},
        {"five", 5},
        {"six", 6},
        {"seven", 7},
        {"eight", 8},
        {"nine", 9}
    };

    int first = -1;
    int last  = -1;

    for(auto match : ctre::range<pattern>(line))
    {
        const int num = [&]() -> int {
            const auto view = match.to_view();

            if(view == "oneight")
            {
                if(first == -1)
                {
                    return 1;
                }
                else
                {
                    return 8;
                }
            }
            else if(view == "twone")
            {
                if(first == -1)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            else if(view == "eightwo")
            {
                if(first == -1)
                {
                    return 8;
                }
                else
                {
                    return 2;
                }
            }
            else if(str_to_num.contains(view))
            {
                return str_to_num.at(match.to_view());
            }
            else
            {
                return match.to_number();
            }
        }();

        if(first < 0)
        {
            first = num;
        }

        last = num;
    }

    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
    return first * 10 + last;
}


int Day01::CalcCalibValue(std::istream& ins) const
{
    int accum = 0;

    std::string line;
    while(std::getline(ins, line))
    {
        if(_prms.part == CLIParams::Part::PART1)
        {
            const int inc = ParseDigits(line);
            if(inc > 99 || inc < 11)
            {
                throw std::runtime_error("Bad increment");
            }

            accum += inc;
        }
        else
        {
            const int inc = ParseNumbers(line);
            if(inc > 99 || inc < 11)
            {
                throw std::runtime_error("Bad increment");
            }

            accum += inc;
        }
    }

    return accum;
}

} // namespace advent
