# Advent of Code 2023

My solutions for [Advent of Code 2023](https://adventofcode.com/2023) in C++


## Preparing for the Next Day

1. From project directory run:
    ```sh
    python3 scripts/new_day.py <DayNum>
    ```
1. Add `DayXXTest.cpp` to `tests/CMakeLists.txt`
1. Reconfigure
1. Put puzzle name in `src/DayXX.hpp`
1. Rebuild


## Usage

`build/bin/aoc23 <day_num> <input_file> [--part2]`


## Build Instructions

From the project directory:

```sh
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..
cmake --build . --config [Release|Debug]
```
