#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <vector>

#include <fmt/format.h>

#include "Day06.hpp"


TEST_CASE("Day 6 NumRecords()", "[Day06]")
{
    struct TestCase
    {
        uint64_t time;
        uint64_t distance;
        uint64_t count;
    };

    const std::vector<TestCase> cases{{7, 9, 4}, {15, 40, 8}, {30, 200, 9}};


    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY06;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day06 day{prms};


    for(const auto& test : cases)
    {
        CHECK(day.NumRecords(test.time, test.distance) == test.count);
    }
}


TEST_CASE("Day 6; parse input", "[Day06]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY06;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day06 day{prms};


    const std::vector<uint64_t> values{10, 21, 23};
    const std::string           line{
        fmt::format("text:    {} {} {}", values.at(0), values.at(1), values.at(2))
    };


    const std::vector<uint64_t> rslts = day.ParseInts(line);


    CHECK(rslts == values);
}
