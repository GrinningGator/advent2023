#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <sstream>

#include <fmt/format.h>

#include "Day07.hpp"


CATCH_REGISTER_ENUM(
    advent::Day07::HandType,
    advent::Day07::HandType::FiveOfAKind,
    advent::Day07::HandType::FourOfAKind,
    advent::Day07::HandType::FullHouse,
    advent::Day07::HandType::ThreeOfAKind,
    advent::Day07::HandType::TwoPair,
    advent::Day07::HandType::OnePair,
    advent::Day07::HandType::HighCard,
    advent::Day07::HandType::Unknown
);


TEST_CASE("Day 7; CalcType()", "[Day07]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day07 day{prms};

    SECTION("Five of a Kind")
    {
        const char card = GENERATE('A', 'T');

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card);
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::FiveOfAKind);
    }

    SECTION("Four of a Kind")
    {
        const char   card1 = GENERATE('A', 'T');
        const char   card2 = '5';
        const size_t pos   = GENERATE(0, 2, 4);

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card1);
        cards.at(pos) = card2;
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::FourOfAKind);
    }

    SECTION("Full House")
    {
        const char   card1 = GENERATE('A', 'T');
        const char   card2 = '5';
        const size_t pos2a = GENERATE(2, 4);
        const size_t pos2b = 1;

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card1);
        cards.at(pos2a) = card2;
        cards.at(pos2b) = card2;
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::FullHouse);
    }

    SECTION("Three of a Kind")
    {
        const char   card1 = GENERATE('A', 'T');
        const char   card2 = '5';
        const char   card3 = '6';
        const size_t pos2  = GENERATE(2, 4);
        const size_t pos3  = 0;

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card1);
        cards.at(pos2) = card2;
        cards.at(pos3) = card3;
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::ThreeOfAKind);
    }

    SECTION("Two Pair")
    {
        const char card1 = GENERATE('A', 'T');
        const char card2 = '5';
        const char card3 = '6';

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card1);
        cards.at(0) = card2;
        cards.at(2) = card2;
        cards.at(3) = card3;
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::TwoPair);
    }

    SECTION("One Pair")
    {
        const char card1 = GENERATE('A', 'T');
        const char card2 = '5';
        const char card3 = '6';
        const char card4 = '7';

        std::array<char, advent::Day07::hand_size> cards{};
        cards.fill(card1);
        cards.at(0) = card2;
        cards.at(2) = card3;
        cards.at(4) = card4;
        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::OnePair);
    }


    SECTION("High Card")
    {
        const std::array<char, advent::Day07::hand_size> cards{'2', '3', '4', '5', '6'};

        CAPTURE(cards);

        CHECK(day.CalcHandType(cards) == advent::Day07::HandType::HighCard);
    }
}


TEST_CASE("Day 7; parsing", "[Day07]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day07 day{prms};

    const std::array<char, advent::Day07::hand_size> cards{'2', '3', '4', '5', '6'};

    const std::uint64_t bid_value = 465ull;
    const std::string   hand_str{cards.begin(), cards.end()};
    const std::string   line{fmt::format("{} {}", hand_str, bid_value)};

    const advent::Day07::Hand hand = day.ParseHand(line);

    CHECK(hand.bid == bid_value);
    CHECK(hand.cards == cards);
    CHECK(hand.type == advent::Day07::HandType::HighCard);
}


TEST_CASE("Day 7; comparing hands", "[Day07]")
{
    using Hand     = advent::Day07::Hand;
    using HandType = advent::Day07::HandType;

    const Hand five_of_a_kind1{{'T', 'T', 'T', 'T', 'T'}, 100, HandType::FiveOfAKind};
    const Hand five_of_a_kindK{{'K', 'K', 'K', 'K', 'K'}, 100, HandType::FiveOfAKind};
    const Hand four_of_kind_p0{{'T', 'K', 'K', 'K', 'K'}, 100, HandType::FourOfAKind};
    const Hand four_of_kind_p4{{'K', 'K', 'K', 'K', 'T'}, 100, HandType::FourOfAKind};
    const Hand full_house{{'T', 'T', 'T', '2', '2'}, 100, HandType::FullHouse};
    const Hand three_of_a_kind{{'K', 'K', 'K', 'T', '2'}, 100, HandType::ThreeOfAKind};
    const Hand two_pair{{'K', 'K', 'T', 'T', '2'}, 100, HandType::TwoPair};
    const Hand one_pair{{'K', 'K', 'T', '2', '3'}, 100, HandType::OnePair};
    const Hand high_card{{'T', '2', '3', '4', '5'}, 100, HandType::HighCard};


    /*
            FiveOfAKind,
            FourOfAKind,
            FullHouse,
            ThreeOfAKind,
            TwoPair,
            OnePair,
            HighCard,
    */

    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, four_of_kind_p0) == false);

    CHECK(advent::Day07::HandLessThan(full_house, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(full_house, four_of_kind_p0) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, full_house) == false);
    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, full_house) == false);

    CHECK(advent::Day07::HandLessThan(three_of_a_kind, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(three_of_a_kind, four_of_kind_p0) == true);
    CHECK(advent::Day07::HandLessThan(three_of_a_kind, full_house) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, three_of_a_kind) == false);
    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, three_of_a_kind) == false);
    CHECK(advent::Day07::HandLessThan(full_house, three_of_a_kind) == false);

    CHECK(advent::Day07::HandLessThan(two_pair, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(two_pair, four_of_kind_p0) == true);
    CHECK(advent::Day07::HandLessThan(two_pair, full_house) == true);
    CHECK(advent::Day07::HandLessThan(two_pair, three_of_a_kind) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, two_pair) == false);
    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, two_pair) == false);
    CHECK(advent::Day07::HandLessThan(full_house, two_pair) == false);
    CHECK(advent::Day07::HandLessThan(three_of_a_kind, two_pair) == false);

    CHECK(advent::Day07::HandLessThan(one_pair, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(one_pair, four_of_kind_p0) == true);
    CHECK(advent::Day07::HandLessThan(one_pair, full_house) == true);
    CHECK(advent::Day07::HandLessThan(one_pair, three_of_a_kind) == true);
    CHECK(advent::Day07::HandLessThan(one_pair, two_pair) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, one_pair) == false);
    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, one_pair) == false);
    CHECK(advent::Day07::HandLessThan(full_house, one_pair) == false);
    CHECK(advent::Day07::HandLessThan(three_of_a_kind, one_pair) == false);
    CHECK(advent::Day07::HandLessThan(two_pair, one_pair) == false);

    CHECK(advent::Day07::HandLessThan(high_card, five_of_a_kind1) == true);
    CHECK(advent::Day07::HandLessThan(high_card, four_of_kind_p0) == true);
    CHECK(advent::Day07::HandLessThan(high_card, full_house) == true);
    CHECK(advent::Day07::HandLessThan(high_card, three_of_a_kind) == true);
    CHECK(advent::Day07::HandLessThan(high_card, two_pair) == true);
    CHECK(advent::Day07::HandLessThan(high_card, one_pair) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, high_card) == false);
    CHECK(advent::Day07::HandLessThan(four_of_kind_p0, high_card) == false);
    CHECK(advent::Day07::HandLessThan(full_house, high_card) == false);
    CHECK(advent::Day07::HandLessThan(three_of_a_kind, high_card) == false);
    CHECK(advent::Day07::HandLessThan(two_pair, high_card) == false);
    CHECK(advent::Day07::HandLessThan(one_pair, high_card) == false);

    // compare matching kinds card by card
    CHECK(advent::Day07::HandLessThan(five_of_a_kind1, five_of_a_kindK) == true);
    CHECK(advent::Day07::HandLessThan(five_of_a_kindK, five_of_a_kind1) == false);
}


TEST_CASE("Day 7; sorting hands", "[Day07]")
{
    using Hand     = advent::Day07::Hand;
    using HandType = advent::Day07::HandType;


    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day07 day{prms};

    const Hand five_of_a_kindK{{'K', 'K', 'K', 'K', 'K'}, 100, HandType::FiveOfAKind};
    const Hand five_of_a_kind1{{'T', 'T', 'T', 'T', 'T'}, 100, HandType::FiveOfAKind};
    const Hand four_of_a_kind{{'T', 'K', 'K', 'K', 'K'}, 100, HandType::FourOfAKind};
    const Hand three_of_a_kind{{'K', 'K', 'K', 'T', '2'}, 100, HandType::ThreeOfAKind};
    const Hand one_pair{{'K', 'K', 'T', '2', '3'}, 100, HandType::OnePair};

    std::vector<Hand> hands{
        five_of_a_kindK, four_of_a_kind, five_of_a_kind1, three_of_a_kind, one_pair
    };

    const std::vector<Hand> sorted_hands{
        one_pair, three_of_a_kind, four_of_a_kind, five_of_a_kind1, five_of_a_kindK
    };


    day.SortHands(hands);


    for(size_t i = 0; i < hands.size(); ++i)
    {
        CAPTURE(i);

        CHECK(hands.at(i).type == sorted_hands.at(i).type);
        CHECK(hands.at(i).cards == sorted_hands.at(i).cards);
    }
}


TEST_CASE("Day 7 - calculate winnings", "[Day07]")
{
    using Hand     = advent::Day07::Hand;
    using HandType = advent::Day07::HandType;

    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day07 day{prms};

    const std::vector<Hand> hands{
        {{'K', 'K', 'T', '2', '3'}, 1, HandType::OnePair},
        {{'K', 'K', 'K', 'T', '2'}, 2, HandType::ThreeOfAKind},
        {{'T', 'K', 'K', 'K', 'K'}, 3, HandType::FourOfAKind}
    };

    const uint64_t exp_winnings = (1 * 1) + (2 * 2) + (3 * 3);

    CHECK(day.CalcTotalWinnings(hands) == exp_winnings);
}
