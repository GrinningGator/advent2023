#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <sstream>
#include <string>

#include "Day01.hpp"

TEST_CASE("ParseDigits()", "[Day01]")
{
    const advent::CLIParams prms;
    const advent::Day01     app{prms};

    SECTION("1abc2")
    {
        const std::string input = "1abc2\n";

        CHECK(app.ParseDigits(input) == 12);
    }

    SECTION("pqr3stu8vwx")
    {
        const std::string input = "pqr3stu8vwx\n";

        CHECK(app.ParseDigits(input) == 38);
    }

    SECTION("a1b2c3d4e5f")
    {
        const std::string input = "a1b2c3d4e5f\n";

        CHECK(app.ParseDigits(input) == 15);
    }

    SECTION("treb7uchet")
    {
        const std::string input = "treb7uchet\n";

        CHECK(app.ParseDigits(input) == 77);
    }
}


TEST_CASE("Calibration Value - part 1", "[Day01]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY01;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day01 app{prms};

    const std::string  input{"1abc2\n"
                             "pqr3stu8vwx\n"
                             "a1b2c3d4e5f\n"
                             "treb7uchet\n"};
    std::istringstream ins{input};

    CHECK(app.CalcCalibValue(ins) == 142);
}


TEST_CASE("Parse Numbers", "[Day01]")
{
    const advent::CLIParams prms;
    const advent::Day01     app{prms};

    SECTION("two1nine")
    {
        const std::string input = "two1nine\n";

        CHECK(app.ParseNumbers(input) == 29);
    }

    SECTION("eightwothree")
    {
        const std::string input = "eightwothree\n";

        CHECK(app.ParseNumbers(input) == 83);
    }

    SECTION("abcone2threexyz")
    {
        const std::string input = "abcone2threexyz\n";

        CHECK(app.ParseNumbers(input) == 13);
    }

    SECTION("xtwone3four")
    {
        const std::string input = "xtwone3four\n";

        CHECK(app.ParseNumbers(input) == 24);
    }

    SECTION("4nineeightseven2")
    {
        const std::string input = "4nineeightseven2\n";

        CHECK(app.ParseNumbers(input) == 42);
    }

    SECTION("zoneight234")
    {
        const std::string input = "zoneight234\n";

        CHECK(app.ParseNumbers(input) == 14);
    }

    SECTION("7pqrstsixteen")
    {
        const std::string input = "7pqrstsixteen\n";

        CHECK(app.ParseNumbers(input) == 76);
    }


    // from test.txt
    SECTION("zqkgbnqsixlqlbbhpdkvfourfive71")
    {
        const std::string input = "zqkgbnqsixlqlbbhpdkvfourfive71\n";

        CHECK(app.ParseNumbers(input) == 61);
    }

    SECTION("nine5four3twoneg")
    {
        const std::string input = "nine5four3twoneg\n";

        CHECK(app.ParseNumbers(input) == 91);
    }

    SECTION("nineeightjlngjz94t7")
    {
        const std::string input = "nineeightjlngjz94t7\n";

        CHECK(app.ParseNumbers(input) == 97);
    }


    // single number words
    SECTION("one")
    {
        const std::string input = "one\n";

        CHECK(app.ParseNumbers(input) == 11);
    }

    SECTION("two")
    {
        const std::string input = "two\n";

        CHECK(app.ParseNumbers(input) == 22);
    }

    SECTION("three")
    {
        const std::string input = "three\n";

        CHECK(app.ParseNumbers(input) == 33);
    }

    SECTION("four")
    {
        const std::string input = "four\n";

        CHECK(app.ParseNumbers(input) == 44);
    }

    SECTION("five")
    {
        const std::string input = "five\n";

        CHECK(app.ParseNumbers(input) == 55);
    }

    SECTION("six")
    {
        const std::string input = "six\n";

        CHECK(app.ParseNumbers(input) == 66);
    }

    SECTION("seven")
    {
        const std::string input = "seven\n";

        CHECK(app.ParseNumbers(input) == 77);
    }

    SECTION("eight")
    {
        const std::string input = "eight\n";

        CHECK(app.ParseNumbers(input) == 88);
    }

    SECTION("nine")
    {
        const std::string input = "nine\n";

        CHECK(app.ParseNumbers(input) == 99);
    }

    // edge cases
    SECTION("eightwo1")
    {
        const std::string input = "eightwo1\n";

        CHECK(app.ParseNumbers(input) == 81);
    }

    SECTION("1eightwo")
    {
        const std::string input = "1eightwo\n";

        CHECK(app.ParseNumbers(input) == 12);
    }

    SECTION("oneight1")
    {
        const std::string input = "oneight1\n";

        CHECK(app.ParseNumbers(input) == 11);
    }

    SECTION("1oneight")
    {
        const std::string input = "1oneight\n";

        CHECK(app.ParseNumbers(input) == 18);
    }

    SECTION("twone1")
    {
        const std::string input = "twone1\n";

        CHECK(app.ParseNumbers(input) == 21);
    }

    SECTION("1twone")
    {
        const std::string input = "1twone\n";

        CHECK(app.ParseNumbers(input) == 11);
    }
}


TEST_CASE("Calibration Value - part 2", "[Day01]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY01;
    prms.part = advent::CLIParams::Part::PART2;

    const advent::Day01 app{prms};

    const std::string  input{"two1nine\n"
                             "eightwothree\n"
                             "abcone2threexyz\n"
                             "xtwone3four\n"
                             "4nineeightseven2\n"
                             "zoneight234\n"
                             "7pqrstsixteen\n"};
    std::istringstream ins{input};

    CHECK(app.CalcCalibValue(ins) == 281);
}