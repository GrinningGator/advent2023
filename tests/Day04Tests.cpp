#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <sstream>

#include "Day04.hpp"

TEST_CASE("Day 4 -- split sets from line", "[Day04]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY04;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day04 day{prms};

    const std::string line = "Card 100: 10 20 30 | 40 50 6 700";

    const auto [winning, have] = day.SplitLine(line);

    CHECK(winning.size() == 3);
    CHECK(winning.contains(10));
    CHECK(winning.contains(20));
    CHECK(winning.contains(30));

    CHECK(have.size() == 4);
    CHECK(have.contains(40));
    CHECK(have.contains(50));
    CHECK(have.contains(6));
    CHECK(have.contains(700));
}


TEST_CASE("Day 4, intersection", "[Day04]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY04;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day04 day{prms};

    const std::set<int> winning{1, 2, 3, 4};
    const std::set<int> tickets{2, 3, 5, 6};

    const std::set<int> intersect = day.Intersect(winning, tickets);

    CHECK(intersect.size() == 2);
    CHECK(intersect.contains(2));
    CHECK(intersect.contains(3));
}
