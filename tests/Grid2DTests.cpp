#include <catch2/catch_test_macros.hpp>

#include "Grid2D.hpp"


TEST_CASE("Grid 2D Loading / Accessing", "[Common]")
{
    advent::Grid2D grid;
    const size_t   num_cols = 5;
    const size_t   num_rows = 4;

    auto char_at = [num_cols](size_t col, size_t row) -> char {
        static constexpr char ascii_min_print = 'A';

        const char ncols = static_cast<char>(num_cols);
        const char xchar = static_cast<char>(col);
        const char ychar = static_cast<char>(row);

        return static_cast<char>(ascii_min_print + (ncols * ychar) + xchar);
    };

    for(size_t row = 0; row < num_rows; ++row)
    {
        std::string line;
        for(size_t col = 0; col < num_cols; ++col)
        {
            line.push_back(char_at(col, row));
        }

        grid.LoadRow(line);
    }


    REQUIRE(grid.NumCols() == num_cols);
    REQUIRE(grid.NumRows() == num_rows);

    for(size_t row = 0; row < num_rows; ++row)
    {
        for(size_t col = 0; col < num_cols; ++col)
        {
            CAPTURE(col, row);
            CHECK(grid.At(col, row) == char_at(col, row));
        }
    }
}
