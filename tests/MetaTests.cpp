#include <catch2/catch_test_macros.hpp>

// test cases to make sure unit test infrastructure is working
// These are hidden and will not run by default,
// run test exe with command line argument '[.]' to run these tests

TEST_CASE("Passing Test", "[.meta]")
{
    REQUIRE(true);
}

TEST_CASE("Failing Test", "[.meta][!shouldfail]")
{
    REQUIRE(false);
}
