#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <fmt/format.h>

#include "Day03.hpp"


TEST_CASE("Day 3 Is Symbol", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day03 day{prms};

    for(char chr = '!'; chr < std::numeric_limits<char>::max(); ++chr)
    {
        CAPTURE(chr);

        const bool is_symbol = (chr != '.' && (chr < '0' || chr > '9'));
        CHECK(day.IsSymbol(chr) == is_symbol);
    }
}


TEST_CASE("Day 3 Is Digit", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day03 day{prms};

    for(char chr = '!'; chr < std::numeric_limits<char>::max(); ++chr)
    {
        CAPTURE(chr);

        const bool is_digit = (chr >= '0' && chr <= '9');
        CHECK(day.IsDigit(chr) == is_digit);
    }
}


TEST_CASE("Day 3 LoadGrid()", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day03 day{prms};

    const std::string lines{".....\n"
                            ".....\n"
                            ".....\n"};
    const size_t      n_cols{5};
    const size_t      n_rows{3};

    std::istringstream ins{lines};

    day.LoadGrid(ins);

    CHECK(day.Grid().NumCols() == n_cols);
    CHECK(day.Grid().NumRows() == n_rows);
}


TEST_CASE("Day 3 GetNumber() - adjacent symbol", "[Day03]")
{
    static const std::string left{".......\n"
                                  "./123..\n"
                                  ".......\n"};
    static const std::string right{".......\n"
                                   "..123+.\n"
                                   ".......\n"};
    static const std::string up1{"..*....\n"
                                 "..123..\n"
                                 ".......\n"};
    static const std::string up2{"...-...\n"
                                 "..123..\n"
                                 ".......\n"};
    static const std::string up3{"..../..\n"
                                 "..123..\n"
                                 ".......\n"};
    static const std::string up4{".....-.\n"
                                 "..123..\n"
                                 ".......\n"};
    static const std::string up5{".+.....\n"
                                 "..123..\n"
                                 ".......\n"};
    static const std::string down1{".......\n"
                                   "..123..\n"
                                   "..+....\n"};
    static const std::string down2{".......\n"
                                   "..123..\n"
                                   "...*...\n"};
    static const std::string down3{".......\n"
                                   "..123..\n"
                                   "..../..\n"};
    static const std::string down4{".......\n"
                                   "..123..\n"
                                   ".....-.\n"};
    static const std::string down5{".......\n"
                                   "..123..\n"
                                   "./.....\n"};

    const int    number = 123;
    const size_t row    = 1;
    const size_t col    = 2;

    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day03 day{prms};

    const std::string lines =
        GENERATE_REF(left, right, up1, up2, up3, up4, up5, down1, down2, down3, down4, down5);


    std::istringstream ins{lines};

    day.LoadGrid(ins);

    CHECK(day.GetNumber(col, row) == number);
}

TEST_CASE("Day 3 GetNumber() - no adjacent symbol", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day03 day{prms};

    const std::string lines{".......\n"
                            "..123..\n"
                            ".......\n"};
    const size_t      row = 1;
    const size_t      col = 2;


    std::istringstream ins{lines};

    day.LoadGrid(ins);

    CHECK(day.GetNumber(col, row) == 0);
}

TEST_CASE("Day 3 GetNumber() - left right edges", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day03 day{prms};

    const std::string lines{"...\n"
                            "123\n"
                            "...\n"};
    const size_t      row = 1;
    const size_t      col = 0;


    std::istringstream ins{lines};

    day.LoadGrid(ins);

    CHECK(day.GetNumber(col, row) == 0);
}

TEST_CASE("Day 3 GetNumber() - top bottom edges", "[Day03]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day03 day{prms};

    const std::string lines{"..123..\n"};
    const size_t      row = 0;
    const size_t      col = 2;


    std::istringstream ins{lines};

    day.LoadGrid(ins);

    CHECK(day.GetNumber(col, row) == 0);
}


TEST_CASE("Day 3, Part 2 Sum", "[Day03][.ignore]")
{
    static const std::string test1{"467.\n"
                                   "...*\n"
                                   "..35\n"};
    static const std::string test2{".755\n"
                                   "*...\n"
                                   "598.\n"};
    static const std::string test2b{".755\n"
                                    ".*..\n"
                                    "598.\n"};
    static const std::string test3{"612.507\n"
                                   "...*...\n"};
    static const std::string test3b{"...*...\n"
                                    "612.507\n"};
    static const std::string test4{"10*20\n"};
    static const std::string test5{"796*...\n"
                                   "....294\n"};
    static const std::string test6{"796....\n"
                                   "...*294\n"};
    static const std::string test7{"796.....\n"
                                   "....*294\n"};


    using TestPair = std::pair<std::string, int>;

    const std::vector<TestPair> tests{
        TestPair(test1, 467 * 35),
        TestPair(test2, 755 * 598),
        TestPair(test2b, 755 * 598),
        TestPair(test3, 612 * 507),
        TestPair(test3b, 612 * 507),
        TestPair(test4, 10 * 20),
        TestPair(test5, 796 * 294),
        TestPair(test6, 796 * 294),
        TestPair(test7, 0)
    };


    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY03;
    prms.part = advent::CLIParams::Part::PART2;

    for(const auto& [lines, rslt] : tests)
    {
        advent::Day03 day{prms};

        std::istringstream ins{lines};

        day.LoadGrid(ins);

        CHECK(day.Part2Sum() == rslt);
    }
}
