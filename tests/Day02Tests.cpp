#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <array>
#include <utility>

#include <fmt/format.h>

#include "Day02.hpp"

TEST_CASE("Parse Hand", "[Day02]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY02;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day02 day{prms};

    SECTION("Red, Green, Blue, with ;")
    {
        const int         red   = 20;
        const int         blue  = 5;
        const int         green = 8;
        const std::string str{fmt::format("{} red, {} blue, {} green;", red, blue, green)};

        const advent::RGB hand = day.ParseHand(str);

        CHECK(hand.red == red);
        CHECK(hand.green == green);
        CHECK(hand.blue == blue);
    }

    SECTION("Blue, Green, Red, end of line")
    {
        const int         red   = 20;
        const int         blue  = 5;
        const int         green = 8;
        const std::string str{fmt::format("{} blue, {} green, {} red", blue, green, red)};

        const advent::RGB hand = day.ParseHand(str);

        CHECK(hand.red == red);
        CHECK(hand.green == green);
        CHECK(hand.blue == blue);
    }

    SECTION("Green, Red, with ;")
    {
        const int         red   = 20;
        const int         blue  = 0;
        const int         green = 8;
        const std::string str{fmt::format("{} green, {} red;", green, red)};

        const advent::RGB hand = day.ParseHand(str);

        CHECK(hand.red == red);
        CHECK(hand.green == green);
        CHECK(hand.blue == blue);
    }
}


TEST_CASE("ParseGame", "[Day02]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY02;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day02 day{prms};

    const int game_id = 2;

    const std::array<advent::RGB, 3> targets{
        advent::RGB{0, 1, 2}, advent::RGB{1, 3, 4}, advent::RGB{0, 1, 1}
    };

    const std::string str = fmt::format(
        "Game {}: {} blue, {} green; {} green, {} blue, {} red; {} green, {} blue",
        game_id,
        targets.at(0).blue,
        targets.at(0).green,
        targets.at(1).green,
        targets.at(1).blue,
        targets.at(1).red,
        targets.at(2).green,
        targets.at(2).blue
    );

    const advent::Game game = day.ParseGame(str);

    // check game ID
    CHECK(game.id == game_id);


    // check game hands
    REQUIRE(game.hands.size() == targets.size());

    for(size_t i = 0; i < game.hands.size(); ++i)
    {
        CHECK(game.hands.at(i).red == targets.at(i).red);
        CHECK(game.hands.at(i).green == targets.at(i).green);
        CHECK(game.hands.at(i).blue == targets.at(i).blue);
    }
}


TEST_CASE("IsGamePossible", "[Day02]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY02;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day02 day{prms};


    advent::Game game;
    game.id = 1;
    game.hands.emplace_back(advent::RGB{10, 5, 5});
    game.hands.emplace_back(advent::RGB{5, 10, 5});

    const advent::RGB possible_contents{10, 10, 10};
    const advent::RGB impossible_contents1{5, 10, 10};
    const advent::RGB impossible_contents2{10, 5, 10};

    CHECK(day.IsGamePossible(game, possible_contents) == true);
    CHECK(day.IsGamePossible(game, impossible_contents1) == false);
    CHECK(day.IsGamePossible(game, impossible_contents2) == false);
}


TEST_CASE("Day 2 Part 1 Test Cases", "[Day02]")
{
    using test_pair = std::pair<std::string_view, bool>;

    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY02;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day02 day{prms};

    const test_pair test_case = GENERATE(
        test_pair{"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", true},
        test_pair{"Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", true},
        test_pair{
            "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red", false
        },
        test_pair{
            "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red", false
        },
        test_pair{"Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", true}
    );
    const auto [line, rslt] = test_case;

    const advent::RGB contents{12, 13, 14};

    const auto game = day.ParseGame(line);

    CHECK(day.IsGamePossible(game, contents) == rslt);
}


TEST_CASE("Day 2 Part 2 Test Cases", "[Day02]")
{
    using RGB       = advent::RGB;
    using test_pair = std::pair<std::string_view, RGB>;

    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY02;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day02 day{prms};

    const test_pair test_case = GENERATE(
        test_pair{"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", RGB{4, 2, 6}},
        test_pair{"Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", RGB{1, 3, 4}},
        test_pair{
            "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
            RGB{20, 13, 6}
        },
        test_pair{
            "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
            RGB{14, 3, 15}
        },
        test_pair{"Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", RGB{6, 3, 2}}
    );
    const auto [line, rslt] = test_case;

    const advent::RGB contents{12, 13, 14};

    const auto game = day.ParseGame(line);

    const RGB min_cubes = day.MinCubes(game);

    CHECK(min_cubes.red == rslt.red);
    CHECK(min_cubes.green == rslt.green);
    CHECK(min_cubes.blue == rslt.blue);
}
