#include <string_view>

#include <catch2/catch_test_macros.hpp>

#include <ctre.hpp>


TEST_CASE("Digit", "[.hidden][ctre]")
{
    static constexpr auto  pattern = ctll::fixed_string{"[0-9]"};
    const std::string_view strv    = "9";

    const auto rslt = ctre::search<pattern>(strv);

    CHECK((bool)rslt == true);

    CHECK(rslt.get<0>().to_number() == 9);
    CHECK(rslt.get<0>().to_string() == "9");
}


TEST_CASE("Digit with chars", "[.hidden][ctre]")
{
    static constexpr auto  pattern = ctll::fixed_string{"([0-9])"};
    const std::string_view strv    = "x9x";

    const auto rslt = ctre::search<pattern>(strv);

    CHECK((bool)rslt == true);

    CHECK(rslt.count() == 2); // from docs "whole regex_results is implicit capture 0"

    CHECK(rslt.get<0>().to_number() == 9);
    CHECK(rslt.get<0>().to_string() == "9");
}


TEST_CASE("Multiple digits with chars", "[.hidden][ctre]")
{
    static constexpr auto  pattern = ctll::fixed_string{"([0-9])"};
    const std::string_view strv    = "x9x8x7x6x";

    int count = 0;
    for(auto match : ctre::range<pattern>(strv))
    {
        CHECK(match.to_number() == (9 - count));

        ++count;
    }

    CHECK(count == 4);
}


TEST_CASE("Multiple numbers and words", "[.hidden][ctre]")
{
    static constexpr auto  pattern = ctll::fixed_string{"(one|two|[0-9])"};
    const std::string_view strv    = "one8two7";

    int count = 0;
    for(auto match : ctre::range<pattern>(strv))
    {
        switch(count)
        {
            case 0: CHECK(match.to_string() == "one"); break;
            case 1: CHECK(match.to_number() == 8); break;
            case 2: CHECK(match.to_string() == "two"); break;
            case 3: CHECK(match.to_number() == 7); break;
            default: CHECK(false);
        };

        ++count;
    }

    CHECK(count == 4);
}


TEST_CASE("Parsing Games", "[.hidden][ctre]")
{
    static constexpr auto game_id = ctll::fixed_string{"Game (\\d+):"};
    static constexpr auto hand    = ctll::fixed_string{"(?<=[:;])(.+?)(?=[;\\n])"};


    const std::string line = "Game 10: 10 red, 10 blue; 10 green, 10 red\n";

    const auto game_match = ctre::search<game_id>(line);

    CHECK(game_match.get<1>().to_number() == 10);


    int hand_idx = 0;
    for(const auto match : ctre::range<hand>(line))
    {
        switch(hand_idx)
        {
            case 0: CHECK(match.get<1>().to_string() == " 10 red, 10 blue"); break;
            case 1: CHECK(match.get<1>().to_string() == " 10 green, 10 red"); break;
            default: CHECK(false);
        };

        ++hand_idx;
    }
}
