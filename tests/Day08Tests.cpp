#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <string>

#include <fmt/format.h>

#include "Day08.hpp"

TEST_CASE("Day 8: Parse Node", "[Day08]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day08 day{prms};


    const std::string node_name  = GENERATE("AAA", "ZZZ");
    const std::string left_name  = GENERATE("BBB", "YYY");
    const std::string right_name = GENERATE("CCC", "XXX");
    const std::string line       = fmt::format("{} = ({}, {})", node_name, left_name, right_name);


    const auto [name, nodes] = day.ParseNode(line);

    CHECK(name == node_name);
    CHECK(nodes.left == left_name);
    CHECK(nodes.right == right_name);
}


TEST_CASE("Day 8: Parse Node with digits", "[Day08]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY07;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day08 day{prms};


    const std::string node_name  = GENERATE("11A", "22Z");
    const std::string left_name  = GENERATE("11B", "22Y");
    const std::string right_name = GENERATE("11C", "22X");
    const std::string line       = fmt::format("{} = ({}, {})", node_name, left_name, right_name);


    const auto [name, nodes] = day.ParseNode(line);

    CHECK(name == node_name);
    CHECK(nodes.left == left_name);
    CHECK(nodes.right == right_name);
}