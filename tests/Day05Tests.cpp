#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <sstream>

#include <fmt/format.h>

#include "Day05.hpp"

TEST_CASE("Day05 ParseNumbers()", "[Day05]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY05;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day05 day{prms};

    const std::string line{"0 10 20 30 40"};

    const std::vector<uint64_t> values = day.ParseNumbers(line);


    CHECK(values.size() == 5);

    for(size_t i = 0; i < values.size(); ++i)
    {
        CHECK(values.at(i) == static_cast<uint64_t>(i * 10));
    }
}


TEST_CASE("Day05 ParseNumbers() - big numbers", "[Day05]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY05;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day05 day{prms};

    static constexpr uint64_t value0 = 2369327568ull;
    static constexpr uint64_t value1 = 3429737174ull;
    static constexpr uint64_t value2 = 127508203ull;
    const std::string         line{fmt::format("{} {} {}", value0, value1, value2)};

    const std::vector<uint64_t> values = day.ParseNumbers(line);


    REQUIRE(values.size() == 3);
    CHECK(values.at(0) == value0);
    CHECK(values.at(1) == value1);
    CHECK(values.at(2) == value2);
}


TEST_CASE("Day05 ParseRange()", "[Day05]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY05;
    prms.part = advent::CLIParams::Part::PART1;

    const advent::Day05 day{prms};

    const uint64_t    dest   = 1000000ull;
    const uint64_t    source = 2000000ull;
    const uint64_t    size   = 3000000ull;
    const std::string line{fmt::format("{} {} {}", dest, source, size)};

    const advent::Range range = day.ParseRange(line);

    CHECK(range.destStart == dest);
    CHECK(range.sourceStart == source);
    CHECK(range.range == size);
}


TEST_CASE("Day 5; Map", "[Day05]")
{
    static constexpr advent::Range range1{1ull, 10ull, 5ull};
    static constexpr advent::Range range2{100ull, 1000ull, 50ull};

    const advent::Map map{std::vector<advent::Range>{range1, range2}};

    // inside range
    CHECK(map.At(range1.sourceStart + range1.range / 2) == range1.destStart + range1.range / 2);
    CHECK(map.At(range2.sourceStart + range2.range / 2) == range2.destStart + range2.range / 2);
    CHECK(map.At(range1.sourceStart) == range1.destStart);
    CHECK(map.At(range2.sourceStart) == range2.destStart);

    // outside of range, input = destination
    CHECK(map.At(range1.sourceStart + range1.range + 1) == range1.sourceStart + range1.range + 1);
    CHECK(map.At(range2.sourceStart + range2.range + 1) == range2.sourceStart + range2.range + 1);
    CHECK(map.At(range2.sourceStart - 1) == range2.sourceStart - 1);
    CHECK(map.At(range2.sourceStart - 1) == range2.sourceStart - 1);
}


TEST_CASE("Day 5; ParseFile() / MapSeed()", "[Day05]")
{
    advent::CLIParams prms;
    prms.day  = advent::CLIParams::Day::DAY05;
    prms.part = advent::CLIParams::Part::PART1;

    advent::Day05 day{prms};

    const std::string  lines{"seeds: 79 14 55 13\n"
                             "\n"
                             "seed-to-soil map:\n"
                             "50 98 2\n"
                             "52 50 48\n"
                             "\n"
                             "soil-to-fertilizer map:\n"
                             "0 15 37\n"
                             "37 52 2\n"
                             "39 0 15\n"
                             "\n"
                             "fertilizer-to-water map:\n"
                             "49 53 8\n"
                             "0 11 42\n"
                             "42 0 7\n"
                             "57 7 4\n"
                             "\n"
                             "water-to-light map:\n"
                             "88 18 7\n"
                             "18 25 70\n"
                             "\n"
                             "light-to-temperature map:\n"
                             "45 77 23\n"
                             "81 45 19\n"
                             "68 64 13\n"
                             "\n"
                             "temperature-to-humidity map:\n"
                             "0 69 1\n"
                             "1 0 69\n"
                             "\n"
                             "humidity-to-location map:\n"
                             "60 56 37\n"
                             "56 93 4\n"};
    std::istringstream ins{lines};


    day.ParseInput(ins);

    const std::vector<std::pair<uint64_t, uint64_t>> tests{{79, 82}, {14, 43}, {55, 86}, {13, 35}};

    for(const auto [seed, location] : tests)
    {
        CAPTURE(seed, location);
        CHECK(day.LocationFromSeed(seed) == location);
    }
}
